import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/**
 * @program: guli_parent
 * @description: s
 * @author: Mr.shen
 * @create: 2020-06-13 08:51
 **/
public class lettt {
    @Test
    public void test(){
        int i = this.pivotIndex(new int[]{1, 7, 3, 6, 5, 6});
        System.out.println(i);
    }

    public boolean isUnique(String str){
        HashSet<Character> set = new HashSet<>();
        for (int i = 0; i < str.length(); i++) {
            set.add(str.charAt(i));
        }
        return set.size()== str.length() ? true : false;
    }

    public int[] plusOne(int[] digits) {
        for (int i = digits.length - 1; i >= 0; i--) {
            digits[i]++;
            digits[i] = digits[i] % 10;
            if (digits[i] != 0) return digits;
        }
        digits = new int[digits.length + 1];
        digits[0] = 1;
        return digits;
    }
    public int pivotIndex(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            int sumLeft = 0;
            int sumRight = 0;
            for (int j = 0; j < i; j++) {
                sumLeft += nums[j];
            }
            for (int k = i + 1; k < nums.length; k++) {
                sumRight += nums[k];
            }
            if (sumLeft == sumRight) {
                return i;
            }
        }
        return -1;
    }

    @Test
    public void tests(){
//        [[1,3],[2,6],[8,10],[15,18]]
        int[][] nums = new int[][] {{1,3},{2,6},{8,10},{15,18}};
        int[][] merge = this.merge(nums);
        for (int i = 0; i < merge.length; i++) {
            for (int i1 = 0; i1 < merge.length; i1++) {
                System.out.println(merge[i][i1]);
            }
        }
    }

    public int searchInsert(int[] nums,int target){
        for (int i = 0; i < nums.length; i++) {
            if(nums[i]==target){
                return i;
            }
        }
        for (int i = 0; i < nums.length; i++) {
            if(nums[i] == 0){
                nums[i] = target;
                return i;
            }
        }
        return -1;
    }

    public int[][] merge(int[][] intervals){
        List<int[]> res = new ArrayList<>();
        if (intervals.length == 0) return res.toArray(new int[0][]);
        Arrays.sort(intervals, (a, b) -> a[0] - b[0]);
        int i = 0;
        while (i < intervals.length) {
            int left = intervals[i][0];
            int right = intervals[i][1];
            //可以用连续的不止两个数组重合，所以用while循环
            while (i < intervals.length - 1 && intervals[i][0] <= right) {
                i++;
                right = Math.max(right, intervals[i][1]);
            }
            res.add(new int[]{left, right});
            i++;
        }
        return res.toArray(new int[0][]);
    }

}
