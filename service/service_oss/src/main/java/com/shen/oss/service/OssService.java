package com.shen.oss.service;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

/**
 * @program: guli_parent
 * @description: 业务逻辑
 * @author: Mr.shen
 * @create: 2020-06-14 19:26
 **/
public interface OssService {
    /**
     * 上传头像到Oss
     * @return String
     */
    String uploadFileAvatar(MultipartFile file);
}
