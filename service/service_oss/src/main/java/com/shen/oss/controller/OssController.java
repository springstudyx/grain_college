package com.shen.oss.controller;

import com.shen.commonutils.R;
import com.shen.oss.service.OssService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @program: guli_parent
 * @description: Oss控制器
 * @author: Mr.shen
 * @create: 2020-06-14 19:25
 **/
@RestController
@RequestMapping("/eduoss/fileoss")
@CrossOrigin
public class OssController {
    @Autowired
    private OssService service;
    /**
     * 上传头像的方法
     * */
    @PostMapping("/uploadOssFile")
    public R uploadOssFile(MultipartFile file){
        //获取上传文件
        String Url = service.uploadFileAvatar(file);
        return R.ResultOk().data("url",Url);
    }
}
