package com.shen.demo;

import com.alibaba.excel.EasyExcel;
import com.shen.demo.excel.DemoData;
import com.shen.demo.excel.ExcelListener;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: guli_parent
 * @description: 测试
 * @author: Mr.shen
 * @create: 2020-06-15 12:19
 **/
public class EasyExcelTest {
    public static void main(String[] args) {
        //实现Excel读
        String fileName = "E:\\write.xlsx";
        EasyExcel.read(fileName,DemoData.class,new ExcelListener()).sheet().doRead();
    }

    public static void wite(){
        //实现excel写的操作
        //设置写入文件夹地址和excel文件名称
        String fileName = "E:\\write.xlsx";

        //调用EasyExcel里面的方法
        EasyExcel.write(fileName, DemoData.class).sheet("学生列表").doWrite(getData());
    }

    //创建方法返回List
    private static List<DemoData> getData(){
        List<DemoData> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            DemoData data = new DemoData();
            data.setSno(i);
            data.setSname("lucy"+i);
            list.add(data);
        }
        return list;
    }

    @Test
    public void s(){
        Object a = 10;
        int b = (int) a;
    }
}
