package com.shen.edu.service;

import com.shen.edu.entity.Subject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.shen.edu.entity.subject.OneSubject;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 课程科目 服务类
 * </p>
 *
 * @author shen
 * @since 2020-06-15
 */
public interface SubjectService extends IService<Subject> {

    /**
     * 添加课程分类
     */
    void saveSubject(MultipartFile file,SubjectService subjectService);

    /**
     * 返回科目列表 树形
     * @return
     */
    List<OneSubject> getAllOneTwoSubject();
}
