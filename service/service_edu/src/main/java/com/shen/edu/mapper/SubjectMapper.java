package com.shen.edu.mapper;

import com.shen.edu.entity.Subject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程科目 Mapper 接口
 * </p>
 *
 * @author shen
 * @since 2020-06-15
 */
public interface SubjectMapper extends BaseMapper<Subject> {

}
