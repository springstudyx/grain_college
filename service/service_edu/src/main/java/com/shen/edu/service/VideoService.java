package com.shen.edu.service;

import com.shen.edu.entity.Video;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程视频 服务类
 * </p>
 *
 * @author shen
 * @since 2020-06-21
 */
public interface VideoService extends IService<Video> {

}
