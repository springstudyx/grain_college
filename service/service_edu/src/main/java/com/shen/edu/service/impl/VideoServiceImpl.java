package com.shen.edu.service.impl;

import com.shen.edu.entity.Video;
import com.shen.edu.mapper.VideoMapper;
import com.shen.edu.service.VideoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 课程视频 服务实现类
 * </p>
 *
 * @author shen
 * @since 2020-06-21
 */
@Service
public class VideoServiceImpl extends ServiceImpl<VideoMapper, Video> implements VideoService {

}
