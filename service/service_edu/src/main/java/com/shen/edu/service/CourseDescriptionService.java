package com.shen.edu.service;

import com.shen.edu.entity.CourseDescription;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程简介 服务类
 * </p>
 *
 * @author shen
 * @since 2020-06-21
 */
public interface CourseDescriptionService extends IService<CourseDescription> {

}
