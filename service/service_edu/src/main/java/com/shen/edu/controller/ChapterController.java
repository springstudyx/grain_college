package com.shen.edu.controller;


import com.shen.commonutils.R;
import com.shen.edu.entity.chapter.ChapterVo;
import com.shen.edu.service.ChapterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author shen
 * @since 2020-06-21
 */
@RestController
@RequestMapping("/eduservice/chapter")
@CrossOrigin
public class ChapterController {
    @Autowired
    private ChapterService chapterService;

    @GetMapping("/getChapter/{courseId}")
    public R getChapterService(@PathVariable String courseId){
        List<ChapterVo> list = chapterService.getChapterVideoByCourseId(courseId);
        return R.ResultOk().data("allChapterVideo",list);
    }
}

