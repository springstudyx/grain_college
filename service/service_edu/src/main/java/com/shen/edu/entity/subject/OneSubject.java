package com.shen.edu.entity.subject;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: guli_parent
 * @description: 一级分类
 * @author: Mr.shen
 * @create: 2020-06-18 20:14
 **/
@Data
public class OneSubject {
    private String id;
    private String title;

    /**
     * 一个一级分类中可以包含多个二级分类
     */
    private List<TwoSubject> children = new ArrayList<>();
}
