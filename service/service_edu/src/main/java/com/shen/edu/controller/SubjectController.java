package com.shen.edu.controller;


import com.shen.commonutils.R;
import com.shen.edu.entity.subject.OneSubject;
import com.shen.edu.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 课程科目 前端控制器
 * </p>
 *
 * @author shen
 * @since 2020-06-15
 */
@RestController
@RequestMapping("/eduservice/subject")
@CrossOrigin
public class SubjectController {
    @Autowired
    private SubjectService subjectService;

    /**添加课程分类
    获取上传的文件，把文件中的内容提取出来
     */
    @PostMapping("/addSubject")
    public R addSubject(MultipartFile file){
        subjectService.saveSubject(file,subjectService);
        return R.ResultOk();
    }

    /**
     * 课程分类列表(树形)
     * @return
     */
    @GetMapping("getAllSubject")
    public R getAllSubject(){
        List<OneSubject> list = subjectService.getAllOneTwoSubject();
        return R.ResultOk().data("list",list);
    }


}

