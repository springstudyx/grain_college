package com.shen.edu.service.impl;

import com.shen.edu.entity.Teacher;
import com.shen.edu.mapper.TeacherMapper;
import com.shen.edu.service.TeacherService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 讲师 服务实现类
 * </p>
 *
 * @author shen
 * @since 2020-06-07
 */
@Service
public class TeacherServiceImpl extends ServiceImpl<TeacherMapper, Teacher> implements TeacherService {

}
