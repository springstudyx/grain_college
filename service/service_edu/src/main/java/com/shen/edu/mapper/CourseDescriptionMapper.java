package com.shen.edu.mapper;

import com.shen.edu.entity.CourseDescription;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程简介 Mapper 接口
 * </p>
 *
 * @author shen
 * @since 2020-06-21
 */
public interface CourseDescriptionMapper extends BaseMapper<CourseDescription> {

}
