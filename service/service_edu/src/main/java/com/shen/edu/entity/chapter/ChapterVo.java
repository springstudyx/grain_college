package com.shen.edu.entity.chapter;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: guli_parent
 * @description: 章节
 * @author: Mr.shen
 * @create: 2020-06-25 17:48
 **/
@Data
public class ChapterVo {
    /**
     * 章节Id
     */
    private String id;
    /**
     * 章节标题
     */
    private String title;

    /**
     * 表示小节
     */
    private List<VideoVo> children = new ArrayList<>();
}
