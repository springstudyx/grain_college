package com.shen.edu.mapper;

import com.shen.edu.entity.Teacher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 讲师 Mapper 接口
 * </p>
 *
 * @author shen
 * @since 2020-06-07
 */
public interface TeacherMapper extends BaseMapper<Teacher> {

}
