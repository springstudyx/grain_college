package com.shen.edu.entity.subject;

import lombok.Data;

/**
 * @program: guli_parent
 * @description: 二级分类
 * @author: Mr.shen
 * @create: 2020-06-18 20:15
 **/
@Data
public class TwoSubject {
    private String id;
    private String title;
}
