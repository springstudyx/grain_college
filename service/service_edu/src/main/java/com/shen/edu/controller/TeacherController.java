package com.shen.edu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.shen.commonutils.R;
import com.shen.edu.entity.Teacher;
import com.shen.edu.entity.vo.TeacherQuery;
import com.shen.edu.service.TeacherService;
import com.shen.servicebase.execptionhandler.GuliException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *http://localhost:8001/swagger-ui.html
 * @author shen
 * @since 2020-06-07
 */
@Api(tags="讲师管理")
@RestController
@RequestMapping("/eduservice/teacher")
@CrossOrigin
public class TeacherController {
    /**注入
     */
    @Autowired
    private TeacherService teacherService;
    /**1、查询所有讲师内容
    rest风格
     http://localhost:8001/edu/teacher/findAll
    */
    @ApiOperation(value = "所有讲师列表")
    @GetMapping("findAll")
    public R findAllTeacher(){
        List<Teacher> list = teacherService.list(null);
        return R.ResultOk().data("items",list);
    }

    /**
     * 逻辑删除
     * @param id
     * @return
     */
    @ApiOperation(value = "逻辑删除讲师")
    @DeleteMapping("{id}")
    public R removeTeacher(@PathVariable("id") String id){
        boolean flag = teacherService.removeById(id);
        if (flag){
            return R.ResultOk();
        }else{
            return R.ResultError();
        }

    }
    @ApiOperation(value = "分页查询讲师")
    @GetMapping("pageTeacher/{current}/{limit}")
    public R pageListTeacher(@PathVariable("current")Long current,
                             @PathVariable("limit")Long limit){
        try {
            int i = 1/0;
        } catch (Exception e) {
            throw new GuliException(20001,"自定义异常");
        }
        Page<Teacher> pageTeacher = new Page<>(current,limit);
        teacherService.page(pageTeacher,null);
        long total = pageTeacher.getTotal();
        List<Teacher> records = pageTeacher.getRecords();
        return R.ResultOk().data("total",total).data("rows",records);
    }
    @PostMapping("pageTeacherCondition/{current}/{limit}")
    public R pageTeacherCondition(@PathVariable("current")Long current,
                                  @PathVariable("limit")Long limit,
                                  @RequestBody(required = false) TeacherQuery teacherQuery){
        //创建page对象
        Page<Teacher> pageTeacher = new Page<>(current,limit);

        //构建条件
        QueryWrapper<Teacher> wrapper = new QueryWrapper<>();
        // 多条件组合查询
        // mybatis学过 动态sql
        String name = teacherQuery.getName();
        Integer level = teacherQuery.getLevel();
        String begin = teacherQuery.getBegin();
        String end = teacherQuery.getEnd();
        System.out.println(name);
        //判断条件值是否为空，如果不为空拼接条件
        if(!StringUtils.isEmpty(name)) {
            wrapper.like("name",name);
        }
        if(!StringUtils.isEmpty(level)) {
            wrapper.eq("level",level);
        }
        if(!StringUtils.isEmpty(begin)) {
            wrapper.ge("gmt_create",begin);
        }
        if(!StringUtils.isEmpty(end)) {
            wrapper.le("gmt_create",end);
        }

        //排序
        wrapper.orderByDesc("gmt_create");

        //调用方法实现条件查询分页
        teacherService.page(pageTeacher,wrapper);
        /**总记录数*/
        long total = pageTeacher.getTotal();
        /**数据list集合*/
        List<Teacher> records = pageTeacher.getRecords();
        return R.ResultOk().data("total",total).data("rows",records);
    }
    @PostMapping("addTeacher")
    public R addTeacher(@RequestBody Teacher teacher){
        System.out.println("DeBug ===============>" + teacher);
        boolean save = teacherService.save(teacher);
        if (save){
            return R.ResultOk();
        }else{
            return R.ResultError();
        }
    }

    @GetMapping("getTeacher/{id}")
    public R getTeacher(@PathVariable("id") String id){
        Teacher teacher = teacherService.getById(id);
        return R.ResultOk().data("teacher",teacher);
    }

    @PostMapping("updateTeacher")
    public R updateTeacher(@RequestBody Teacher teacher){
        boolean flag = teacherService.updateById(teacher);
        if (flag){
            return R.ResultOk();
        }else{
            return R.ResultError();
        }
    }
}

