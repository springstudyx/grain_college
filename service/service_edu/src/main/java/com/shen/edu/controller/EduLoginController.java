package com.shen.edu.controller;

import com.shen.commonutils.R;
import org.springframework.web.bind.annotation.*;

/**
 * @program: guli_parent
 * @description: 登录
 * @author: Mr.shen
 * @create: 2020-06-11 21:32
 **/
@RestController
@RequestMapping("/eduService/user")
//解决跨域
@CrossOrigin
public class EduLoginController {

    /**
     * login
     * */
    @PostMapping("/login")
    public R login(){

        return R.ResultOk().data("token","admin");
    }
    /**
     * info
     *
     */
    @GetMapping("/info")
    public R info(){
        return R.ResultOk().data("roles","[admin]").data("name","admin").data("avatar","https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif");
    }

}
