package com.shen.edu.service;

import com.shen.edu.entity.Chapter;
import com.baomidou.mybatisplus.extension.service.IService;
import com.shen.edu.entity.chapter.ChapterVo;

import java.util.List;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author shen
 * @since 2020-06-21
 */
public interface ChapterService extends IService<Chapter> {

    List<ChapterVo> getChapterVideoByCourseId(String courseId);
}
