package com.shen.edu.service.impl;

import com.shen.edu.entity.Course;
import com.shen.edu.entity.CourseDescription;
import com.shen.edu.entity.vo.CourseInfoVo;
import com.shen.edu.mapper.CourseMapper;
import com.shen.edu.service.CourseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shen.servicebase.execptionhandler.GuliException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author shen
 * @since 2020-06-21
 */
@Service
public class CourseServiceImpl extends ServiceImpl<CourseMapper, Course> implements CourseService {

    @Autowired
    private CourseDescriptionServiceImpl courseDescriptionService;

    @Override
    public String saveCourseInfo(CourseInfoVo courseInfoVo) {
        /**
         * 1、向课程表添加课程的基本信息
         * 把CourseInfoVo转换成Course
         */
        Course course = new Course();
        BeanUtils.copyProperties(courseInfoVo,course);
        System.out.println(course);
        System.out.println(courseInfoVo);
        int insert = baseMapper.insert(course);
        if(insert <= 0){
            /**
             * 添加失败
             */
            throw new GuliException(20001,"添加失败");
        }
        /**
         * 2、向课程简介添加课程简介
         * edu_course_description
         */
        //获取添加之后课程id
        String cid = course.getId();
        //2 向课程简介表添加课程简介
        //edu_course_description
        CourseDescription courseDescription = new CourseDescription();
        courseDescription.setDescription(courseInfoVo.getDescription());
        //设置描述id就是课程id
        courseDescription.setId(cid);
        courseDescriptionService.save(courseDescription);

        return cid;
    }
}
