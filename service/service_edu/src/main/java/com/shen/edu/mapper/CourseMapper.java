package com.shen.edu.mapper;

import com.shen.edu.entity.Course;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author shen
 * @since 2020-06-21
 */
public interface CourseMapper extends BaseMapper<Course> {

}
