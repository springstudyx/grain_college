package com.shen.edu.mapper;

import com.shen.edu.entity.Video;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程视频 Mapper 接口
 * </p>
 *
 * @author shen
 * @since 2020-06-21
 */
public interface VideoMapper extends BaseMapper<Video> {

}
