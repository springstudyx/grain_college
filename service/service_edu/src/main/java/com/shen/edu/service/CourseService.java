package com.shen.edu.service;

import com.shen.edu.entity.Course;
import com.baomidou.mybatisplus.extension.service.IService;
import com.shen.edu.entity.vo.CourseInfoVo;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author shen
 * @since 2020-06-21
 */
public interface CourseService extends IService<Course> {

    /**
     * 添加课程信息
     * @param courseInfoVo
     */
    String saveCourseInfo(CourseInfoVo courseInfoVo);
}
