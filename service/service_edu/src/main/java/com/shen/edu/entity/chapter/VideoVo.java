package com.shen.edu.entity.chapter;

import lombok.Data;

/**
 * @program: guli_parent
 * @description: 小节
 * @author: Mr.shen
 * @create: 2020-06-25 17:48
 **/
@Data
public class VideoVo {
    /**
     * 小节Id
     */
    private String id;
    /**
     * 小节标题
     */
    private String title;
}
