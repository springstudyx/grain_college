package com.shen.edu.service;

import com.shen.edu.entity.Teacher;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 讲师 服务类
 * </p>
 *
 * @author shen
 * @since 2020-06-07
 */
public interface TeacherService extends IService<Teacher> {

}
