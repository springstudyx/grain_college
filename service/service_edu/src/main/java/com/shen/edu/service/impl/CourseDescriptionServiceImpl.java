package com.shen.edu.service.impl;

import com.shen.edu.entity.CourseDescription;
import com.shen.edu.mapper.CourseDescriptionMapper;
import com.shen.edu.service.CourseDescriptionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 课程简介 服务实现类
 * </p>
 *
 * @author shen
 * @since 2020-06-21
 */
@Service
public class CourseDescriptionServiceImpl extends ServiceImpl<CourseDescriptionMapper, CourseDescription> implements CourseDescriptionService {

}
