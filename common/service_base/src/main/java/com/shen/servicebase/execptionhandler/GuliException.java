package com.shen.servicebase.execptionhandler;

import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: guli_parent
 * @description: 自定义异常处理
 * @author: Mr.shen
 * @create: 2020-06-08 15:01
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GuliException extends RuntimeException {
    /**
     * 异常状态码
     */
    private Integer code;
    /**
     * 异常信息
     */
    private String msg;
}
