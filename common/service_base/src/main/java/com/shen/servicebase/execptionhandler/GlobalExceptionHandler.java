package com.shen.servicebase.execptionhandler;

/**
 * @program: guli_parent
 * @description: 异常统一
 * @author: Mr.shen
 * @create: 2020-06-08 13:58
 **/

import com.shen.commonutils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
    /**
     * 全局异常
     * @param e
     * @return
     */
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public R error(Exception e){
        e.printStackTrace();
        return R.ResultError().message("执行了全局异常处理...");
    }

    /**
     * 特殊异常
     * @param e
     * @return
     */
    @ExceptionHandler(ArithmeticException.class)
    @ResponseBody
    public R arithmeticExceptionError(ArithmeticException e){
        e.printStackTrace();
        return R.ResultError().message("执行了ArithmeticException异常");
    }

    /**
     * 自定义异常处理
     * @param e
     * @return
     */
    @ExceptionHandler(GuliException.class)
    @ResponseBody
    public R error(GuliException e) {
        e.printStackTrace();
        log.error(e.getMsg());
        return R.ResultError().code(e.getCode()).message(e.getMsg());
    }
}
