package com.shen.commonutils;

import io.swagger.models.auth.In;

/**
 * @program: guli_parent
 * @description: 响应状态码
 * @author: Mr.shen
 * @create: 2020-06-07 14:53
 **/
public interface ResultCode {
    /**
     * 成功
     */
    public static Integer SUCCESS = 20000;
    /**
     * 失败
     */
    public static Integer ERROR = 20001;
}
